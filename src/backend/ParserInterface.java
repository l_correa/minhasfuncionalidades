package backend;

/**
 *
 * Parser Interface that turns a {@link String} into a {@link TableData} object.
 * 
 * @author Luciano
 * @author Douglas Vanny Bento
 * 
 */

public interface ParserInterface
{
	/**
	 * 
	 * Exception thrown when the content could not be parsed by the current parser.
	 *
	 */
	public class ContentNotParseable extends Exception
	{
		public ContentNotParseable()
		{
			super("String could not be parsed.");
		}
	}
	
	public TableData parse(String content) throws ContentNotParseable;
}

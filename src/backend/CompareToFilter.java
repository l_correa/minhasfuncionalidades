package backend;

import java.lang.Comparable;

/**
 * 
 * Simple filter to compare objects that implement the {@link java.lang.Comparable} interface.
 *
 * @author Douglas Vanny Bento
 *
 */

public class CompareToFilter implements FilterInterface
{
	public enum Comparison
	{
		EQUAL(0),
		GREATER_THAN(1),
		LESS_THAN(-1),
		GREATER_THAN_OR_EQUAL(2),
		LESS_THAN_OR_EQUAL(-2);
		
		private int value;
		
		private Comparison(int new_value)
		{
			value = new_value;
		}
	}
	
	private Comparable model;
	private Comparison compare_result;
	
	public CompareToFilter(Comparable model_object)
	{
		model = model_object;
		compare_result = Comparison.EQUAL;
	}
	
	public CompareToFilter(Comparable model_object, Comparison compare)
	{
		model = model_object;
		compare_result = compare;
	}
	
	public Boolean isOk(Object obj)
	{
		int ret = model.compareTo(obj);
		
		switch(compare_result)
		{
			case GREATER_THAN_OR_EQUAL:
				return ret != 1;
			case LESS_THAN_OR_EQUAL:
				return ret != -1;
			case GREATER_THAN:
				return ret == -1;
			case LESS_THAN:
				return ret == 1;
			case EQUAL:
				return ret == 0;
		}
		
		return false;
	}
}

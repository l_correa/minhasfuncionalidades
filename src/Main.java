
/**
 *
 * @author Felipe Borges
 *
 */

public class Main {
    public static void main(String args[]) {
        mainWindow window = new mainWindow();
        window.setVisible(true);
        window.setResizable(true);
        window.setRef(window);
    }
}
